package com.kshrd.alertdialog;

import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String[] brand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        brand = getResources().getStringArray(R.array.brand);

        findViewById(R.id.btnAlertBox).setOnClickListener(this);
        findViewById(R.id.btnSingleChoice).setOnClickListener(this);
        findViewById(R.id.btnRadioButtonDialog).setOnClickListener(this);
        findViewById(R.id.btnCheckBoxDialog).setOnClickListener(this);
        findViewById(R.id.btnDialogFragment).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAlertBox:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle("Confirmation")
                        .setMessage("Are you sure to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, "Click Yes", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, "Click No", Toast.LENGTH_SHORT).show();
                            }
                        });
//                        .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
//                            }
//                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;

            case R.id.btnSingleChoice:

                AlertDialog.Builder singleChoiceDialogBuilder = new AlertDialog.Builder(this);
                singleChoiceDialogBuilder
                        .setTitle("Brand")
                        .setItems(brand, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int position) {
                                Toast.makeText(MainActivity.this, brand[position], Toast.LENGTH_SHORT).show();
                            }
                        });
                singleChoiceDialogBuilder.show();
                break;

            case R.id.btnRadioButtonDialog:
                AlertDialog.Builder radioButtonDialogBuilder = new AlertDialog.Builder(this);
                radioButtonDialogBuilder
                        .setTitle("Brand")
                        .setSingleChoiceItems(brand, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int pos) {
                                Toast.makeText(MainActivity.this, brand[pos], Toast.LENGTH_SHORT).show();
                            }
                        });
                radioButtonDialogBuilder.show();
                break;

            case R.id.btnCheckBoxDialog:
                //boolean[] brandCheck = {false, true, false};
                AlertDialog.Builder checkBoxBuilder = new AlertDialog.Builder(this);
                checkBoxBuilder
                        .setTitle("Brand")
                        .setMultiChoiceItems(brand, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int pos, boolean isChecked) {
                                if (isChecked) {
                                    Toast.makeText(MainActivity.this, "Seleced", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Unselected", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                checkBoxBuilder.show();
                break;

            case R.id.btnDialogFragment:
                MyAlertDialog dialogFragment = new MyAlertDialog();
                dialogFragment.show(getSupportFragmentManager(), null);
                break;
        }
    }
}
