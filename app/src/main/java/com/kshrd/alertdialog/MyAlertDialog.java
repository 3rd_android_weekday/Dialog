package com.kshrd.alertdialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by pirang on 5/25/17.
 */

public class MyAlertDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog_fragment, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final EditText etEmail = (EditText) view.findViewById(R.id.etEmail);
        final EditText etPwd = (EditText) view.findViewById(R.id.etPwd);

        view.findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = etEmail.getText().toString();
                String pwd = etPwd.getText().toString();

                if (email.equals("admin@gmail.com") && pwd.equals("12345")){
                    dismiss();
                }
            }
        });

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder
//                .setTitle("Greeting")
//                .setMessage("Hello world!")
//                .setPositiveButton("Close", null);
//        return builder.create();

        // This return statement use for custom dialog
        return super.onCreateDialog(savedInstanceState);
    }
}
